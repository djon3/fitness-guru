﻿///Program Name: Fitness Guru
///Author: Jon Decher
///Date: May 17, 2013
///Description: Allows a user to enter valid information to a WPF GUI application and saves all information in a .txt file

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace FitnessGuru
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        { 
            InitializeComponent();
        }

        //set focus to first textbox on startup
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FNTextBox.Focus();
        }

        //Textboxes - input must be valid character length in order to save 
        private void Firstame_TextBox(object sender, TextChangedEventArgs e)
        {
            if (FNTextBox.Text.Length >= 50)
            {
                errorLabel.Content = "Invalid Input: First name must be less than 50 characters";
                ButtonSave.IsEnabled = false;
            }
            else
            {
                errorLabel.Content = "";
                ButtonSave.IsEnabled = true;
            }
                

            asteriskFn.Visibility = Visibility.Hidden;
        }

        private void LNTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (LNTextBox.Text.Length >= 50){
                errorLabel.Content = "Invalid Input: Last name must be less than 51 characters";
                ButtonSave.IsEnabled = false;
            }
            else
            {
                errorLabel.Content = "";
                ButtonSave.IsEnabled = true;
            }                

            asteriskLn.Visibility = Visibility.Hidden;
        }

        private void TBAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TBAddress.Text.Length >= 50){
                 errorLabel.Content = "Invalid Input: Address must be less than 51 characters";
                 ButtonSave.IsEnabled = false;
            }

            else
            {
                errorLabel.Content = "";
                ButtonSave.IsEnabled = true;
            }          

            asteriskAddress.Visibility = Visibility.Hidden;
        }

        private void TBCity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TBCity.Text.Length >= 50){
                errorLabel.Content = "Invalid Input: City must be less than 51 characters";
                ButtonSave.IsEnabled = false;
            }
            else
            {
                 errorLabel.Content = "";
                 ButtonSave.IsEnabled = true;
            }               

            asteriskCity.Visibility = Visibility.Hidden;
        }

        private void TBPostal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TBPostal.Text.Length > 7){
                 errorLabel.Content = "Invalid Input: Postal Code must be less than 8 characters";
                 ButtonSave.IsEnabled = false;
            }
            else
            {
                 errorLabel.Content = "";
                 ButtonSave.IsEnabled = true;
            }               

            asteriskPc.Visibility = Visibility.Hidden;
        }

        //Buttons
        private void Button_Save(object sender, RoutedEventArgs e)
        {
            //program level 
            string radioChoice, programLevel;

            if (RadioExp.IsChecked == true)
            {
                radioChoice = "Experienced";
                programLevel = radioChoice;
            }
            else if (RadioHigh.IsChecked == true)
            {
                radioChoice = "High Performence";
                programLevel = radioChoice;
            }
            else
            {
                radioChoice = "Beginner";
                programLevel = radioChoice;
            }

            //billing info
            string cmboChoice, billingInfo;

            if (CheckBoxDirect.IsChecked == true)
            {
                cmboChoice = "Bank Account Direct";
                billingInfo = cmboChoice;
            }
            else if (CheckBoxMonthly.IsChecked == true)
            {
                cmboChoice = "Monthly";
                billingInfo = cmboChoice;
            }
            else
            {
                cmboChoice = "Annual Membership Contract";
                billingInfo = cmboChoice;
            }

            //check if all fields are filled
            if (FNTextBox.Text == "" || LNTextBox.Text == "" || cmboYear.SelectedIndex == -1 || cmboMonth.SelectedIndex == -1 || cmboDay.SelectedIndex == -1 ||
                TBAddress.Text == "" || TBCity.Text == "" || cmboProvince.SelectedIndex == -1 || TBPostal.Text == "" || cmboTrainer.SelectedIndex == -1 ||
                CheckBoxDirect.IsChecked == false && CheckBoxMonthly.IsChecked == false && CheckBoxAnnual.IsChecked == false)
            {
                //add an asterisk if the field is blank
                if (FNTextBox.Text == "")
                    asteriskFn.Visibility = Visibility.Visible;
                else
                    asteriskFn.Visibility = Visibility.Hidden;
                if (LNTextBox.Text == "")
                    asteriskLn.Visibility = Visibility.Visible;
                else
                    asteriskLn.Visibility = Visibility.Hidden;
                if (cmboYear.SelectedIndex == -1)
                    asteriskYear.Visibility = Visibility.Visible;
                else
                    asteriskYear.Visibility = Visibility.Hidden;
                if (cmboMonth.SelectedIndex == -1)
                    asteriskMonth.Visibility = Visibility.Visible;
                else
                    asteriskMonth.Visibility = Visibility.Hidden;
                if (cmboDay.SelectedIndex == -1)
                    asteriskDay.Visibility = Visibility.Visible;
                else
                    asteriskDay.Visibility = Visibility.Hidden;
                if (TBAddress.Text == "")
                    asteriskAddress.Visibility = Visibility.Visible;
                else
                    asteriskAddress.Visibility = Visibility.Hidden;
                if (TBCity.Text == "")
                    asteriskCity.Visibility = Visibility.Visible;
                else
                    asteriskCity.Visibility = Visibility.Hidden;
                if (cmboProvince.SelectedIndex == -1)
                    asteriskProv.Visibility = Visibility.Visible;
                else
                    asteriskProv.Visibility = Visibility.Hidden;
                if (TBPostal.Text == "")
                    asteriskPc.Visibility = Visibility.Visible;
                else
                    asteriskPc.Visibility = Visibility.Hidden;
                if (cmboTrainer.SelectedIndex == -1)
                    asteriskTrainer.Visibility = Visibility.Visible;
                else
                    asteriskTrainer.Visibility = Visibility.Hidden;
                if (CheckBoxDirect.IsChecked == false && CheckBoxMonthly.IsChecked == false && CheckBoxAnnual.IsChecked == false)
                    asteriskBi.Visibility = Visibility.Visible;
                else
                    asteriskBi.Visibility = Visibility.Hidden;

                errorLabel.Content = "Please enter all required fields";
            }

            else
            {
                MessageBoxResult saveResult;
                saveResult = MessageBox.Show("Save current information?", "Application Complete", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (MessageBoxResult.Yes == saveResult)
                {

                    //saving to .txt file
                    System.IO.File.WriteAllText("NewMember.txt",
                                                "Fitness Guru" +
                                                "\r\n------------" +
                                                "\r\nFirst Name:    " + FNTextBox.Text +
                                                "\r\nLast Name:     " + LNTextBox.Text +
                                                "\r\nD.O.B.:        " + cmboYear.Text + " / " + cmboMonth.Text + " / " + cmboDay.Text +
                                                "\r\nAddress:       " + TBAddress.Text +
                                                "\r\nCity:          " + TBCity.Text +
                                                "\r\nProvince:      " + cmboProvince.Text +
                                                "\r\nPostal Code:   " + TBPostal.Text +
                                                "\r\nComments:      " + TBComments.Text +
                                                "\r\nTrainer:       " + cmboTrainer.Text +
                                                "\r\nProgram Level: " + programLevel +
                                                "\r\nBilling Info:  " + billingInfo
                    );
                    this.Close();
                }
            }
        }

        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            MessageBoxResult cancelResult;
            cancelResult = MessageBox.Show("Are you sure you want to exit?", "Exit Application", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (MessageBoxResult.Yes == cancelResult)
                this.Close();

        }

        //COMBO BOXES
        enum Months
        {
            Jan = 1,
            Feb,
            Mar,
            Apr,
            May,
            June,
            July,
            Aug,
            Sept,
            Oct,
            Nov,
            Dec
        };
        private void cmboYear_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBoxItem year = new ComboBoxItem();
            int yearMinusSixteen = DateTime.UtcNow.Year - 16;
            int yearIndex = 0;

            for (int i = 1910; i <= yearMinusSixteen; ++i)
            {
                cmboYear.Items.Insert(yearIndex, year.Content = i.ToString());
            }
        }

        private void cmboMonth_Loaded(object sender, RoutedEventArgs e)
        {
            cmboMonth.ItemsSource = Enum.GetNames(typeof(Months));
        }

        enum Provinces
        {
            AB = 1,
            BC,
            MB,
            NB,
            NL,
            NT,
            NS,
            NU,
            ON,
            PE,
            QC,
            SK,
            YT
        };

        private void cmboProvince_Loaded(object sender, RoutedEventArgs e)
        {
            cmboProvince.ItemsSource = Enum.GetNames(typeof(Provinces));
        }

        private void cmboYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            asteriskYear.Visibility = Visibility.Hidden;

            cmboMonth.SelectedIndex = -1;
        }

        private void cmboMonth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmboMonth.SelectedIndex > -1)
                cmboDay.IsEnabled = true;

            asteriskMonth.Visibility = Visibility.Hidden;


            cmboDay.Items.Clear();
            ComboBoxItem day = new ComboBoxItem();
            int index = 0;
            int dayIndex = 0;
            int yearMinusSixteen = DateTime.UtcNow.Year - 16;

            if (cmboMonth.SelectedIndex == 0 || cmboMonth.SelectedIndex == 2 || cmboMonth.SelectedIndex == 4 || cmboMonth.SelectedIndex == 6 ||
            cmboMonth.SelectedIndex == 7 || cmboMonth.SelectedIndex == 9 || cmboMonth.SelectedIndex == 11)
            {
                for (int i = 31; i >= 1; --i)
                    cmboDay.Items.Insert(index, day.Content = i.ToString());
            }
            else if ((0 == ((cmboYear.SelectedIndex - 1) % 4)) && cmboMonth.SelectedIndex == 1)
            {
                for (int x = 29; x >= 1; --x)
                {
                    cmboDay.Items.Insert(dayIndex, day.Content = x.ToString());
                }
            }
            else if (cmboMonth.SelectedIndex == 3 || cmboMonth.SelectedIndex == 5 || cmboMonth.SelectedIndex == 8 || cmboMonth.SelectedIndex == 10)
            {
                for (int i = 30; i >= 1; --i)
                    cmboDay.Items.Insert(index, day.Content = i.ToString());
            }
            else
            {
                for (int x = 28; x >= 1; --x)
                    cmboDay.Items.Insert(dayIndex, day.Content = x.ToString());
            }

        }

        private void cmboDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            asteriskDay.Visibility = Visibility.Hidden;
        }

        enum Trainer
        {
            Tyler = 1,
            Wolf,
            Salem,
            Slater,
            Sam,
            Joey,
            Brayden,
            Mikael,
            Jimmy,
            Steven
        };

        private void cmboTrainer_Loaded(object sender, RoutedEventArgs e)
        {
            cmboTrainer.ItemsSource = Enum.GetNames(typeof(Trainer));
        }

        private void cmboTrainer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            asteriskTrainer.Visibility = Visibility.Hidden;
        }


        //CheckBoxes - allows only one to be checked
        private void CheckBoxDirect_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxDirect.IsChecked == true)
            {
                CheckBoxMonthly.IsChecked = false;
                CheckBoxAnnual.IsChecked = false;
            }
            asteriskBi.Visibility = Visibility.Hidden;
        }

        private void CheckBoxMonthly_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxMonthly.IsChecked == true)
            {
                CheckBoxDirect.IsChecked = false;
                CheckBoxAnnual.IsChecked = false;
            }
            asteriskBi.Visibility = Visibility.Hidden;
        }

        private void CheckBoxAnnual_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxAnnual.IsChecked == true)
            {
                CheckBoxMonthly.IsChecked = false;
                CheckBoxDirect.IsChecked = false;
            }
            asteriskBi.Visibility = Visibility.Hidden;
        }


    }
}

